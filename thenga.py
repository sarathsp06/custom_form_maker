# Enter your code here. Read input from STDIN. Print output to STDOUT
from math import factorial as fact
a=raw_input()
len_by_2=len(a)/2
d=dict()
for i in a:
    if d.has_key(i):
        d[i]+=1
    else:
        d[i] = 1
dinominator = 1
for i in d.values():
	dinominator *=fact(i/2)
print (fact(len_by_2)/dinominator)%1000000007

from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',url(r'^index$','forms.views.index'),
	url(r'^admin/', include(admin.site.urls)),
	url(r'^sign_pro','forms.views.sign'),
	url(r'^signup','forms.views.signup'),
	url(r'^login','forms.views.signin'),
	url(r'^logout','forms.views.signout'),
	url(r'^form-create','forms.views.create'),
	url(r'^do-it','forms.views.makeform'),
	url(r'^form_(?P<fname>[^/]+)','forms.views.show_form'),
	url(r'^view_(?P<fuser>[^/&]+)&(?P<fname>[^/]+)','forms.views.user_form'),
	url(r'^mail-it','forms.views.send_email'),

	)

from django.http import HttpResponse,HttpResponseRedirect
from django.template import RequestContext
from django.shortcuts import render_to_response,get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login,logout
from django.contrib.auth.models import User

def valid(s):
	import re
	if len(re.findall(r'[^a-zA-Z0-9_]',s)) == 0:
		return True
	return False

def escape_and(s):
	if s.find('&error=')!=-1:
		return s[:s.find('&error=')]
	if s.find('&succes=')!=-1:
		return s[:s.find('&succes=')]
	else:
		return s

def index(request):
	
	login = request.user.is_authenticated()
	st=""
	Dictionary = {'user':request.user,'login':0}
	addr = request.get_full_path()
	if addr.find("&error=") != -1:
		Dictionary['error'] = addr[addr.find("&error=")+7:]
	if addr.find("&succes=") != -1:
		Dictionary['succes'] = addr[addr.find("&succes=")+8:]
	if login:
		#if smethingis been send to the index
		if len(request.POST) != 0:
			from forms.models import Form,Question,Answer
			u = request.user
			form = Form.objects.get(name=request.POST['fname'])
			form.votes = form.votes + 1
			for i in sorted(request.POST.keys())[:-3]:
				a=Answer(question=Question.objects.get(pk=i),user=u,answer=request.POST[i])
				a.save()
			form.voter.add(u)
			form.save()
		Dictionary['login']=1
	return render_to_response("forms/home.html",Dictionary,context_instance=RequestContext(request))


def sign(request):#signup
	try:
		username = request.POST['username']
		email = request.POST['email']
		fname=request.POST['fname']
		if valid(username) is False:
			return HttpResponseRedirect("signup&error=invalid username..[a-zA-Z0-9_]are valid")
		lname=request.POST['lname']
		password = request.POST['password']
		user = User.objects.create_user(username,email, password)
		user.first_name=fname
		user.last_name=lname
		user.save()
		return HttpResponseRedirect("index&succes=succesfully signed up")
	except Exception,e:
		return HttpResponseRedirect("signup&error="+e.message)

def signin(request):
	try:
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)	
		if user is not None:
     			login(request, user)
        	  	return HttpResponseRedirect(escape_and(request.META['HTTP_REFERER']))
		else:
    			return HttpResponseRedirect("signup&error=Username or password doesnot exist")#redirect to signup page............		
	except:
		return HttpResponse("Something went wrong..")
	
		
def signout(request):
	logout(request)
	return HttpResponseRedirect(escape_and(request.META['HTTP_REFERER'])+"&succes=succesfully signed off.")

def create(request):#localhost:8000/form-create
	from forms.models import Form,Question,Answer
	try:
		Dictionary = {'user':request.user,'login':0,
	'name':request.POST['name'],'comment':request.POST['comment'],
	'val' : range(int(request.POST['numval'])),
	'num':int(request.POST['numval'])}
		if valid(Dictionary['name']) is False:
			return HttpResponseRedirect("index&error=invalid form name..[a-zA-Z0-9_]are valid")
		if Dictionary['num'] <= 0:
			return HttpResponseRedirect("index&error=Field number can't be negative")
		if request.user.is_authenticated():
			Dictionary['login'] = 1 
			if request.POST['name'] in [i.name for i in Form.objects.all()]:#available already
				return HttpResponseRedirect("index&error=form available with same name")
				
		return render_to_response("forms/form-create.html",Dictionary,context_instance=RequestContext(request))
	except Exception,e:
		return HttpResponseRedirect("index&error="+e.message)
		

def makeform(request):#do-it
	from forms.models import Question,Form
	try:
		D=request.POST
		f = Form(name =D['fname'],comment=D['comment'],votes = 0)
		f.user = request.user
		f.save()
	except Exception,e:
		
		return HttpResponseRedirect("index&error="+e.message)
	try:
		for i in xrange(1,int(D['count'])+1):
			q = Question(question=D['q'+str(i)],form = f,q_type=D['t'+str(i)])
			q.save()
	except Exception, e:
		Question.objects.filter(form=f).delete()
		f.delete()
		return HttpResponseRedirect("index&error="+e.message)
	return HttpResponseRedirect('index&succes=succesfully made form')

def show_form(request,fname):	
	from forms.models import Form,Question
	try:
		dis=0
		form = Form.objects.get(name=fname)
		owner = form.user
		voted = request.user in form.voter.all()
		if owner == request.user or not request.user.is_authenticated() or voted:
			dis = 1
		context = {'form':form,'user':request.user,'voted':voted,'login':0}
		context['disable'] = dis
		if request.user.is_authenticated():
			context['login'] = 1
		addr = request.get_full_path()
		if addr.find("&error=") != -1:
			context['error'] = addr[addr.find("&error=")+7:]
		if addr.find("&succes=") != -1:
			context['succes'] = addr[addr.find("&succes=")+8:]
			print "---------",context['succes']
		return render_to_response('forms/form_view.html',context,context_instance = RequestContext(request))
	except Exception,e:
		return HttpResponseRedirect("index&error="+e.message)
		
def user_form(request,fuser,fname):
	from forms.models import Form,Question,Answer
	try:
		user = request.user
		context = {'user':user}
		context['fuser']=fuser
		if user.is_authenticated():
			context['login'] = 1
		
		form = get_object_or_404(Form,name=fname) 
		fuser = get_object_or_404(User,username=fuser)

		if fuser not in form.voter.all():
			return HttpResponseRedirect("index&error=user not yet voted for the same..")
		elif user != form.user and user != fuser:
			return HttpResponseRedirect("index&error=Only form owner is allowed to view others entry.")
		else:
			questions=form.question_set.all()
		
			context['questions']=questions
			context['form']=form
			return render_to_response('forms/user_form.html',context,context_instance=RequestContext(request))

	except Exception,e:
		return HttpResponseRedirect("index&error=" + e.message)
	



def send_email(request):
	from django.core.mail import EmailMultiAlternatives
	from django.template.loader import render_to_string
	from django.utils.html import strip_tags

	subject = "Form...request to connect "+request.META['HTTP_HOST']+'/form_'+request.POST['fname']
	from_email = "sarath.sp06@gmail.com" 
	
	to_email = request.POST['email']
		
	link = "http://"+request.META['HTTP_HOST']+'/form_'+request.POST['fname']

	html_content = render_to_string("forms/email.html",{'user':request.user,'form':request.POST['fname'],'link':link})
	print html_content
	text_content= strip_tags(html_content)
	
	print link
		
	msg = EmailMultiAlternatives(subject, text_content, from_email, [to_email])
	msg.attach_alternative(html_content,"text/html")
	if subject and html_content and to_email:
		try:
			msg.send()	
		except Exception,e:
			return HttpResponseRedirect(escape_and(request.META['HTTP_REFERER'])+"&error=couldn'd send"+e.message)
		return HttpResponseRedirect(escape_and(request.META['HTTP_REFERER'])+"&succes=succesfully mailed to "+to_email)
   	else:
       	 	return HttpResponseRedirect('index&error=Make sure all fields are entered and valid.')


def signup(request):
	Dictionary={}
	addr = request.get_full_path()
	if addr.find("&error=") != -1:
		Dictionary['error'] = addr[addr.find("&error=")+7:]
	if request.user.is_authenticated():
		return HttpResponseRedirect('index')
	return render_to_response('forms/signup.html',Dictionary,context_instance=RequestContext(request))

from django.db import models
import datetime
from django.utils import timezone
from django.contrib.auth.models import User 

class Form(models.Model):
	user = models.ForeignKey(User)
	voter = models.ManyToManyField(User,related_name='voteduser')
	name = models.CharField(max_length=20,unique=True)
	comment = models.CharField(max_length=100) 
	votes = models.PositiveIntegerField()
	def __unicode__(self):
		return self.name

class Question(models.Model):
	form = models.ForeignKey(Form)	
	Qtype_choices=(('B',"boolean"),('I','Integer'))
	question = models.CharField(max_length=50)
	q_type = models.CharField(max_length=10,choices = Qtype_choices)
	def __unicode__(self):
		return self.question
	def is_boolean(self):
		return self.q_type == 'B'

class Answer(models.Model):
	question = models.ForeignKey(Question)
	user = models.ForeignKey(User)
	answer = models.IntegerField()
	def __unicode__(self):
		return str(self.answer)
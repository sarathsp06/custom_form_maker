#!/usr/bin/python
from django.contrib import admin
from forms.models import Form,Question,Answer
from django.contrib.auth.models import User	

admin.site.register(Form)
admin.site.register(Question)
admin.site.register(Answer)